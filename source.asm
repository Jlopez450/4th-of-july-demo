 include "ramdat.asm"             
 			  dc.l $FFFFE0,   start,     ErrorTrap, ErrorTrap
	          dc.l ErrorTrap, ErrorTrap, ErrorTrap, ErrorTrap
      	      dc.l ErrorTrap, ErrorTrap, ErrorTrap, ErrorTrap
	          dc.l ErrorTrap, ErrorTrap, ErrorTrap, ErrorTrap
	          dc.l ErrorTrap, ErrorTrap, ErrorTrap, ErrorTrap
              dc.l ErrorTrap, ErrorTrap, ErrorTrap, ErrorTrap
	          dc.l ErrorTrap, ErrorTrap, ErrorTrap, ErrorTrap
	          dc.l HBlank,    ErrorTrap, VBlank,    ErrorTrap
      	      dc.l ErrorTrap, ErrorTrap, ErrorTrap, ErrorTrap
      	      dc.l ErrorTrap, ErrorTrap, ErrorTrap, ErrorTrap
              dc.l ErrorTrap, ErrorTrap, ErrorTrap, ErrorTrap
              dc.l ErrorTrap, ErrorTrap, ErrorTrap, ErrorTrap
              dc.l ErrorTrap, ErrorTrap, ErrorTrap, ErrorTrap
              dc.l ErrorTrap, ErrorTrap, ErrorTrap, ErrorTrap
              dc.l ErrorTrap, ErrorTrap, ErrorTrap, ErrorTrap
              dc.l ErrorTrap, ErrorTrap, ErrorTrap, ErrorTrap
              dc.b "SEGA GENESIS    "
              dc.b "(C)JAL JUL 2015 "
              dc.b "A very patriotic Genesis ROM                    "
              dc.b "A very patriotic Genesis ROM                    "
              dc.b "DM 00000000-03"
              dc.w $DEAD        ;checksum
              dc.b "J               "
              dc.l 0
              dc.l ROM_End
              dc.l $FF0000
              dc.l $FFFFFF
              dc.b "    "
              dc.b "    "
              dc.b "    "
              dc.b "            "                           
              dc.b "This program contains blast processing! "
              dc.b "JUE             "
                          
start:      
 	    move.b  $A10001,d0
        andi.b  #$0F,d0
        beq.b   skiptmss       ;branch if oldest MD/GEN
        move.l  #'SEGA',$A14000 ;satisfy the TMSS 
skiptmss:
        clr d1
        clr d2
		clr d3
		clr d4
		clr d5
		clr d6
		clr d7
	
		move.l #$00000000,a1
		move.l #$00000000,a5
		move.b #$00, region		
        move #$2700,sr
	    bsr setup_vdp
		bsr clear_vram
        bsr create_map
		bsr write_cram
		
		lea (shape),a5
		lea ($ff0000),a1
		bsr decompress
		
        move.l  #$40000000,(a3)  ;set VRAM write    
        move.w  #$40FF, d4
		lea ($ff0000),a5
		bsr vram_loop
		
		move.l #$50000003,(a3);vram write $d000
		lea (font),a5
		move.w #$0800,d4
		bsr vram_loop
		
		lea (text),a5
		move.l #$0000E080,d0
		bsr calc_vram
		move.l d0,(a3)
		bsr termtextloop		
		
		lea (text2),a5
		move.l #$0000ED00,d0
		bsr calc_vram
		move.l d0,(a3)
		bsr termtextloop
		
		lea (easter_egg),a5
		move.l #$0000EE00,d0
		bsr calc_vram
		move.l d0,(a3)
		bsr termtextloop		
		
        move.l #$40000010,(a3)   ;write to vsram      
		move.w #$0000,(a4)
		move.w #$0000,(a4)
		lea (music+40),a2
		move #$2300,sr	
		bra loop
loop:
		bsr music_driver
		bra loop
		
check_region:
 	    move.b  $A10001,d0	
        andi.b #$c0,d0
        cmpi.b #$c0,d0
         beq pal
	    move.w #$8174,(a3)
		move.b #$00, region			
		rts
pal:
	    move.w #$817C,(a3)
		move.b #$ff, region			
		rts
		
write_cram:
        move.w #$0017,d4
        lea (palette).l,a6
        move.l #$C0000000,(a3)  ;set VDP to cRAM write
        bra cram_Loop

cram_Loop:               
        move.w (a6)+,(a4)       ;load colours to cRAM
        dbf d4,cram_Loop
        rts
clear_vram:		       
        move.l  #$40000000,(a3)  ;set VRAM write    
        move.w  #$7fff, d4
clear_loop:             
        nop
        move.w  #$0000,(a4)       ;clear vram
        dbf d4,clear_loop
        rts
setup_vdp:
        lea    $C00004.l,a3     ;VDP control port
	    lea    $C00000.l,a4     ;VDP data port
        lea    (VDPSetupArray).l,a5
        move.w #0018,d4         ;loop counter
VDP_Loop:
        move.w (a5)+,(a3)       ;load setup array
        dbf d4,VDP_Loop
        rts  
	       
vram_loop:             
        move.w  (a5)+,(a4)       ;clear vram
        dbf d4,vram_loop
        rts	
		
create_map:                      ;for 320x256 images
        move.l #$0000,d0
        move.w #$002f,d5
        move.l #$60000002,(a3)   ;vram write to $a000	   
superloop:
        move.w #$27,d4
maploop:
        move.w d0,(a4)
        add.w #$1,d0
        dbf d4,maploop
        move.w #$17,d4
maploop2:
        move.w #$05c0,(a4)      ;blank tile
        dbf d4,maploop2
        dbf d5,superloop
        rts	
		
calc_vram:
		move.l d1,-(sp)
		move.l d0,d1
		andi.w #$C000,d1 ;get first two bits only
		lsr.w #$7,d1     ;shift 14 spaces to move it to the end
		lsr.w #$7,d1     ;ditto
		andi.w #$3FFF,d0 ;clear all but first two bits
		eor.w #$4000,d0  ;attach vram write bit
		swap d0          ;move d0 to high word
		eor.w d1,d0      ;smash the two halves together	
		move.l (sp)+,d1
		rts	
		
termtextloop:					;for strings with terminators
		move.b (a5)+,d4	
		cmpi.b #$24,d4			;$ (end of text flag)
		beq return
		andi.w #$00ff,d4
		eor.w #$a000,d4
		add.w #$0680,d4
        move.w d4,(a4)		
		bra termtextloop		
	
return:
		rts
	
ErrorTrap:        
        nop
        bra ErrorTrap

HBlank:                         ;~240 cycles
       add.w #$01,hblanks
       bsr skew_H
       bsr skew_V
       rte
reset_vb:
	   move.w #$0000,vblanks

VBlank:
       add.w #$0002, vblanks	
	   cmpi.w #$0200,vblanks
	    bge reset_vb
	   lea (sine),a1
	   move.l a1,d7
	   add.w vblanks,d7
	   move.l d7,a1   
	   bsr check_region
	   move.w #$0000,hblanks 			
       rte

skew_H:
	   clr d0
	   move.l #$40000003,(a3) ;set vram write at $C000
	   move.w (a1)+,d1
	   lsr.w #$03,d1
	   add.w d1,d0  

	   move.w d0,(a4)		  ;write scroll data
	   move.w #$0000,(a4)	   
	   rts
skew_V:
	   clr d0
	   move.l #$50000010,(a3) ;write to VSRAM
	   move.w (a1),d1
	   lsr.w #$05,d1
	   add.w d1,d0  
	   
	   sub.w #$0008,d0
	   move.w d0,(a4)		  ;write scroll data 
	   move.w #$0000,(a4)	  
	   rts	

	include "music_driver.asm"
	include "decompress.asm"
		
VDPSetupArray:
	dc.w $8014		
	dc.w $8174  ; Genesis mode, DMA enabled, VBLANK-INT enabled		
	dc.w $8228	;field A    
	dc.w $8300	;$833e	
	dc.w $8407	;field B	
	dc.w $8578	;sprite
	dc.w $8600
	dc.w $8700  ;BG color		
	dc.w $8800
	dc.w $8900
	dc.w $8A00		
	dc.w $8B00		
	dc.w $8C81	;81 normal 89 shadow and highlight
	dc.w $8D30  ;h scroll		
	dc.w $8E00
	dc.w $8F02	;auto increment	
	dc.w $9001		
	dc.w $9100		
	dc.w $9200

palette:
	;dc.w $0000,$0E00,$000E,$0EEC,$0EEE,$0E80,$0EE8,$0EE0
	;dc.w $0E60,$0E20,$0EE6,$0C00,$0CEE,$0E8E,$062E,$0000
	
	dc.w $0000,$0C00,$000C,$0EEE,$0000,$0000,$0000,$0000
	dc.w $0000,$0000,$0000,$0000,$0000,$0000,$0000,$0000
	
	dc.w $0000,$0000,$0000,$0EEE,$0000,$0000,$0000,$0000 ;text
	
text:
 dc.b "     Happy 239th birthday, America!$"
text2: 
 dc.b "        Programmed by ComradeOj$"
easter_egg:
 dc.b " PAL is for dirty Redcoats!(just joking)$ " 
	
sine:
	incbin "sine.bin"
	incbin "sine.bin"

shape:
	;incbin "flag.bin"
	;incbin "flag2.bin"
	incbin "flag2.kos"
	
font:
	incbin "ascii.bin"
	
music: ;http://modarchive.org/index.php?request=view_by_moduleid&query=77342	
	incbin "yankee.vgm"

ROM_End:
              
              