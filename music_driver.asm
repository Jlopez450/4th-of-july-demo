music_driver:
 		move.w #$100,($A11100)
		move.w #$100,($A11200)   		
vgm_loop:
		bsr test2612
		clr d2
        move.b (a2)+,d2
		cmpi.b #$61,d2
		 beq wait
		 cmpi.b #$66,d2
		 beq loop_playback
		;cmpi.b #$4f,d2  ;game gear stereo, ignore on GEN/MD
		; beq stereo
		cmpi.b #$52,d2 
		 beq update2612_0
		cmpi.b #$53,d2 
		 beq update2612_1
		cmpi.b #$50,d2
		 beq update_psg
		bra vgm_loop     
update2612_0:
        move.b (a2)+,$A04000
		nop
        move.b (a2)+,$A04001
        bra vgm_loop
		
update2612_1:	
	    move.b (a2)+,$A04002
		nop
        move.b (a2)+,$A04003
		bra vgm_loop
		
loop_playback:
        lea (music+40),a2
		bra vgm_loop
        rts
	
update_psg:
        move.b (a2)+,$C00011
		bra vgm_loop
	
wait:		
		clr d2
		clr d3
		move.b (a2)+,d3
		move.b (a2)+,d2			
		lsl.w #$08,d2    ;switch to big endian
		eor.w d2,d3      ;ditto
		;lsl #$01,d3     ;tempo adjust (removed in favor of more NOPs)
		tst region
		bne waitloop_pal
		bra waitloop
		
waitloop:
		nop
		nop
		nop
		nop
		nop	
		nop		
		nop		
		nop		
		nop
		nop		
		nop
		nop
		nop
		nop
		nop
		nop
		nop	
		dbf d3,waitloop
		bra vgm_loop
		
waitloop_pal: ;two extra NOPs
		nop
		nop
		nop
		nop
		nop
		nop
		nop	
		nop		
		nop		
		nop		
		nop
		nop		
		nop
		nop
		nop
		nop
		nop
		nop
		nop	
		dbf d3,waitloop_pal
		bra vgm_loop		
		
test2612:
		clr d2
        move.b $A04001,d2
		andi.b #$80,d2
        cmpi.b #$80,d2
		beq test2612 
		rts	
		
      